class Analizador {
    outputText = null;
    ramas = null;
    simbols = [
        '<?php',
        'class',
        'function',
        'if',
        'else',
        'for',
        'while',
        'foreach',
        'switch',
        '{',
        '}'
    ]

    tokens = [];
    gramatica = [];

    draw = null;


    VisJS(ramas) {

        console.log(ramas);
        var node = [];
        var edge = [];
        ramas.forEach(rama => {
            if (rama.ant == "root") {

                node.push({
                    id: rama.id,
                    label: 'root: ' + rama.dato
                });
            } else {
                node.push({
                    id: rama.id,
                    label: rama.dato
                });

                edge.push({
                    from: rama.id,
                    to: rama.ant

                });
            }







        });





        var nodes = new vis.DataSet(node);

        // create an array with edges
        var edges = new vis.DataSet(edge);

        // create a network
        var container = document.getElementById("mynetwork");
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {};
        var network = new vis.Network(container, data, options);
    }


    DrawArbol(ramas) {

        var arbol = [];
        ramas.forEach(rama => {
            var array = [];
            array.push(rama);
            if (arbol[rama.nivel] == undefined) {
                arbol[rama.nivel] = array;
            } else {
                arbol[rama.nivel] = [...arbol[rama.nivel], ...array];

            }

        });


        arbol.forEach((element, fila) => {

            var hr = document.createElement('hr');
            this.draw.appendChild(hr);

            if (element != undefined || element != 0) {
                element.forEach(a => {
                    var button = document.createElement("button");
                    if (a.ant == 'root') {

                        button.innerHTML += '<-?php';

                    } else if (a.ant == 0) {
                        button.innerHTML += '<span class="badge badge-rama">root</span>';
                        button.innerHTML += a.dato;
                        button.innerHTML += '<span class="badge badge-id">' + a.id + '</span>';
                    } else {
                        button.innerHTML += '<span class="badge badge-rama">' + a.ant + '</span>';
                        button.innerHTML += a.dato;
                        button.innerHTML += '<span class="badge badge-id">' + a.id + '</span>';
                    }

                    button.style.marginRight = fila * 10 + 'px';
                    button.style.marginLeft = (arbol.length - fila) * 10 + 'px';
                    button.id = a.id;
                    this.draw.appendChild(button);


                });
            }

        });

    }


    Ramas(values) {

        var nivel = 0;
        var niveles = [];
        var ramas = [];

        values.forEach((val, index) => {

            if (val == '<?php' && index == 0) {
                ramas.push({ id: ramas.length, dato: val, ant: 'root', nivel: nivel });
                nivel++;
                niveles[nivel] = ramas.length - 1;

            } else if (val == '{') {
                nivel++;

            } else if (val == '}') {
                nivel--;
            } else {

                ramas.push({ id: ramas.length, dato: val, ant: niveles[nivel], nivel: nivel });
                niveles[nivel + 1] = ramas.length - 1;
            }

        });

        // console.log(niveles);
        // console.log(ramas);
        this.ramas = ramas;
        // this.DrawArbol(ramas);
        this.VisJS(ramas);

    }


    Gramatica() {


        var f = [];

        this.gramatica.forEach((e, index) => {
            var cont = 0;

            this.gramatica.forEach((element, i) => {
                if (i > index && e == element) {
                    cont++;
                }
            });
            if (cont == 0) {

                console.log('e: ', e);

                f.push(e);
            }


        });


        console.log('gramatica: ', f);

        return f;


    }


    VerificarFilla(fila) {
        var collection = [];

        var res = [];

        this.simbols.forEach(simbolo => {
            if (fila.indexOf(simbolo) > -1) {
                if (simbolo == 'function' || simbolo == 'class') {
                    collection[fila.indexOf(simbolo)] = fila;
                } else {
                    collection[fila.indexOf(simbolo)] = simbolo;
                }

            }
        });

        collection.forEach(element => {
            if (element != 0) {
                this.tokens.push(element);
                this.gramatica.push(element);
            }
        });







    }

    Arbol(code) {

        this.gramatica = [];
        this.tokens = [];
        this.tokens = [];
        var cod = code.split('\n');
        var res = [];

        cod.forEach(fila => {
            if (fila !== '') {

                this.VerificarFilla(fila);
            }
        });

        console.log(this.tokens);
        this.Ramas(this.tokens);

    }
    Sintactico(formData, txt_white) {
        this.outputText.value = "";
        fetch('https://phpcodechecker.com/api/', {
            // mode: "no-cors",
            method: 'POST',
            // headers: new Headers(),
            body: formData
        }).then((res) => res.json())
            .then((data) => {


                console.log('data api: ', data);

                if (data.errors == 'FALSE') {
                    this.outputText.value = this.outputText.value + '\nErrors: none';

                } else if (data.errors == 'TRUE') {

                    this.outputText.value = this.outputText.value + '\n';
                    this.outputText.value = this.outputText.value + 'Code: ' + data.syntax.code + '\n';
                    this.outputText.value = this.outputText.value + 'Message: ' + data.syntax.message + '\n';

                } else {
                    this.outputText.value = this.outputText.value + '\nErrors: -';
                }

                this.Arbol(txt_white);
                this.outputText.value = this.outputText.value + '\nLexer: ' + JSON.stringify(this.ramas) + '\n';

                this.outputText.value = this.outputText.value + '\nGrammar:\n S -> <?php P ?>';

                this.Gramatica().forEach(element => {
                    if (element != '<?php') {
                        this.outputText.value = this.outputText.value + '\n P -> ' + element + ' P';
                    }

                });

                this.outputText.value = this.outputText.value + '\n P -> e';


            }
            )
            .catch((err) => console.log(err))


    }

}

var white_area = document.querySelector('#white_txt_area');
var btn_syntax = document.querySelector('#btn_syntax');
var btn_tree = document.querySelector('#btn_tree');
var btn_grammar = document.querySelector('#btn_grammar');
var black_txt = document.querySelector("#black_txt_area");
var btn_x = document.querySelector('#btn_x');
var form = document.querySelector('#postData');
var tree = document.querySelector('#tree');
var txt_fake = document.querySelector('#txt_fake');
btn_x.addEventListener('click', () => {

    black_txt.value = ''
    white_area.value = ' ';
    txt_fake.value = '';
})
const analizador = new Analizador();
analizador.draw = document.querySelector('#draw');
analizador.outputText = black_txt;




form.addEventListener('submit', (e) => {
    e.preventDefault()
    const formData = new FormData(e.currentTarget)

    if (white_area.value != 0) {

        analizador.Sintactico(formData, white_area.value);

    } else {
        black_txt.value = '';

    }
})



$(document).ready(function () {


    window.myCodeMirror = CodeMirror.fromTextArea(document.getElementById("txt_fake"), {
        lineNumbers: true
    });

    window.myCodeMirror.on('change', editor => {
        white_area.value = editor.getValue();
    });

    window.btn_x.addEventListener(('click'), (e) => {
        window.myCodeMirror.value = ''
    })

});

